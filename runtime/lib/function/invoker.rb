# frozen_string_literal: true

require 'json'

module Function
  class Invoker
    def initialize
      @handler = Function::Handler.new(ENV['FUNCTION_HANDLER'])
    end

    def call(env)
      @rack_request = Rack::Request.new(env)

      return not_found unless allowed_method?

      request = Function::Request.new(@rack_request)
      response = @handler.call(event: request.params, context: env)

      [200, { 'Content-Type' => 'application/json' }, [JSON.dump(response)]]
    end

    private

    def not_found
      [404, {}, ['Did you get lost?']]
    end

    def allowed_method?
      @rack_request.get? || @rack_request.post?
    end
  end
end
