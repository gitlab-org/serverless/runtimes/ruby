# frozen_string_literal: true

require 'json'

module Function
  class Request
    def initialize(rack_request)
      @request = rack_request
    end

    def params
      rack_params.to_h.merge(request_payload)
    end

    private

    def rack_params
      @request.params
    end

    def request_payload
      return {} unless @request.content_type == 'application/json'

      JSON.parse(@request.body.read)
    end
  end
end
