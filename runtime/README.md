# Ruby function invoker

This project is used to build a Docker image of a simple Rack Application that runs Puma HTTP server.

## How does it work

* Runs on port 8080.
* It only listens for GET and POST requests. Other methods return 404 response.
* It does not route, thus any path provided will invoke a function.
* Success (200) responses are always `Content-Type:application/json'` and `JSON.dump(response)`. Where `response` is the output of your function.
* Autoloads all Ruby files found in a top-level directory.
* It expects `FUNCTION_HANDLER` environment variable to be defined with a method name to be called.
* Functions will be called with arguments containing `event` and `context` keys as hashes, where:

    1. `event`: will contain a request payload. JSON payload and query arguments are merged together.
    1. `context`: will contain Rack env.

### Examples of functions

1. Object scoped functions

```ruby
# my_function.rb

def hello(event:, context:)
  "Hello World"
end
```

In this case we need to set environment variable `FUNCTION_HANDLER=hello`

2. Class scoped functions

```ruby
# foo.rb

class Foo
  def self.bar(event:, context:)
    "Hello World"
  end
end
```

In this case we need to set environment variable `FUNCTION_HANDLER="Foo.bar"`

### How to test it locally

1. Create your function and put it in a top-level directory:

```ruby
# my-function.rb

class Foo
  def self.run!(event:, context:)
    "Message is #{event["message"]}"
  end
end
```

2. Install dependencies and start the Rack server:

```bash
bundle install
FUNCTION_HANDLER="Foo.run!" rackup -p 8080
```

3. Call your function:

```bash
curl -v http://localhost:8080/?message=my-message
```
