# frozen_string_literal: true

require 'byebug'
require 'rack'
require 'tempfile'
require 'simplecov'

require_relative '../lib/function/invoker'
require_relative '../lib/function/loader'
require_relative '../lib/function/handler'
require_relative '../lib/function/request'

SimpleCov.start

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
