# frozen_string_literal: true

describe Function::Invoker do
  let(:mock_request) do
    Rack::MockRequest.new(described_class.new)
  end

  context 'empty FUNCTION_HANDLER' do
    subject { mock_request.get('') }

    it { expect { subject }.to raise_error(ArgumentError) }
  end

  context 'valid FUNCTION_HANDLER' do
    let(:rack_response) do
      [200, { 'Content-Type' => 'application/json' }, ['"Hello"']]
    end
    let(:function_method_name) { 'bar' }

    before do
      load(function_tempfile)
      allow(ENV).to receive(:[]).and_call_original
      allow(ENV).to receive(:[]).with('FUNCTION_HANDLER').and_return(handler)
    end

    shared_examples_for 'GET or POST request' do
      let(:expected_event_params) { { 'foo' => 'bar', 'key_1' => 'value_1' } }

      context 'with Object scoped function' do
        let(:handler) { function_method_name }
        let(:function_tempfile) do
          file_content = <<-FUNCTION
            def #{function_method_name}(event:, context:)
              'Hello'
            end
          FUNCTION

          file = Tempfile.new(['foo'], __dir__)
          file.write(file_content)
          file.close

          file.path
        end

        before do
          expect_any_instance_of(Object)
            .to receive(handler)
            .with(event: expected_event_params, context: instance_of(Hash))
            .and_call_original
        end

        it { expect(subject.body).to eq('"Hello"') }
        it { expect(subject.status).to eq(200) }
        it { expect(subject.content_type).to eq('application/json') }
      end

      context 'with Class scoped function' do
        let(:function_class) { 'Foo' }
        let(:handler) { [function_class, function_method_name].join('.') }
        let(:function_tempfile) do
          file_content = <<-FUNCTION
          class #{function_class}
            def self.#{function_method_name}(event:, context:)
              'Hello'
            end
          end
          FUNCTION

          file = Tempfile.new(['foo'], __dir__)
          file.write(file_content)
          file.close

          file.path
        end

        before do
          expect(Kernel.const_get(function_class))
            .to receive('send')
            .with(function_method_name,
                  event: expected_event_params,
                  context: instance_of(Hash))
            .and_call_original
        end

        it { expect(subject.body).to eq('"Hello"') }
        it { expect(subject.status).to eq(200) }
        it { expect(subject.content_type).to eq('application/json') }
      end
    end

    context 'GET request' do
      subject { mock_request.get('/?foo=bar&key_1=value_1') }

      let(:event) { nil }

      it_behaves_like 'GET or POST request'
    end

    context 'POST request' do
      subject { mock_request.post('/?foo=bar', input: 'key_1=value_1') }

      let(:event) { {} }

      it_behaves_like 'GET or POST request'
    end
  end
end
