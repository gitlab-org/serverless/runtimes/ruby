# frozen_string_literal: true

require 'spec_helper'

describe 'Rack app config.ru' do
  subject do
    Rack::Builder.parse_file('config.ru')
  end

  context 'when function handler is not defined' do
    it 'raises an expcetion' do
      expect { subject }.to raise_error ArgumentError
    end
  end

  context 'when function handler is defined' do
    before do
      allow(ENV).to receive(:[])
        .and_call_original

      allow(ENV).to receive(:[])
        .with('FUNCTION_HANDLER')
        .and_return('my_function')
    end

    it 'correctly runs the app' do
      expect(subject.first).to be_a Function::Invoker
    end
  end
end
