# frozen_string_literal: true

require 'spec_helper'
require 'puma'
require 'puma/configuration'
require 'net/http'

describe 'Puma lowlevel_error_handler' do
  before(:all) do
    app = ->(_) { raise 'Sample error' }

    @server =
      Puma::Server.new(app, Puma::Events.strings, Puma::Configuration.new.load)
                  .tap { |server| server.add_tcp_listener '127.0.0.1', 0 }
                  .tap(&:run)
  end

  after(:all) do
    @server.stop(true)
  end

  it 'responds with JSON' do
    req = Net::HTTP::Get.new('/')

    res = make_request(req)

    expect(res.header['Content-Type']).to eq('application/json')

    expect(res.body).to eq('{"message":"An unhandled error of type ' \
                              'RuntimeError occurred"}')
  end

  it 'prints details of the error to STDOUT' do
    req = Net::HTTP::Get.new('/')

    expectation = expect { make_request(req) }

    expectation.to output(/"errorType":"RuntimeError"/).to_stdout
    expectation.to output(/"errorMessage":"Sample error"/).to_stdout
    expectation.to output(/"stackTrace":".*"/).to_stdout
  end

  def make_request(req)
    Net::HTTP.start '127.0.0.1', @server.connected_port do |http|
      http.request(req)
    end
  end
end
