# frozen_string_literal: true

lowlevel_error_handler do |e|
  puts JSON.dump(
    'errorType' => e.class,
    'errorMessage' => e.message,
    'stackTrace' => e.backtrace.join("\n")
  )

  [
    500,
    { 'Content-Type' => 'application/json' },
    [JSON.dump(message: "An unhandled error of type #{e.class} occurred")]
  ]
end
